<!DOCTYPE html>
<html>
<title>CV..... </title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="asset/prof.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="icon" type="image/png" href="img/ic.jpg">
<style>
html,body,h1,h2,h3,h4,h5,h6 {font-family: monospace }
</style>
<body class="fajar-light-grey">

<!-- Page Container -->
<div class="fajar-content fajar-margin-top" style="max-width:1400px;">

  <!-- The Grid -->
  <div class="fajar-row-padding">
  
    <!-- Left Column -->
    <div class="fajar-third">
    
      <div class="fajar-white fajar-text-grey fajar-card-4">
        <div class="fajar-display-container">
          <img src="img/foto.jpg" style="width:100%" alt="Avatar">
          <div class="fajar-display-bottomleft fajar-container fajar-text-black">
            <h2>Anis Fajar Prakoso</h2>
          </div>
        </div>
        <div class="fajar-container">
          <p><i class="fa fa-code fa-fw fajar-margin-right fajar-large fajar-text-teal"></i>Programmer</p>
          <p><i class="fa fa-home fa-fw fajar-margin-right fajar-large fajar-text-teal"></i>Kudus, Indonesia</p>
          <p><i class="fa fa-envelope fa-fw fajar-margin-right fajar-large fajar-text-teal"></i>anisfajar.kudus@gmail.com</p>
          <p><i class="fa fa-phone fa-fw fajar-margin-right fajar-large fajar-text-teal"></i>085-727-109-286</p>
          <hr>

          <p class="fajar-large"><b><i class="fa fa-asterisk fa-fw fajar-margin-right fajar-text-teal"></i>Skills</b></p>
          <p>PHP Programming</p>
          <div class="fajar-light-grey fajar-round-xlarge fajar-small">
            <div class="fajar-container fajar-center fajar-round-xlarge fajar-teal" style="width:80%">80%</div>
          </div>
          <p>C# Programming</p>
          <div class="fajar-light-grey fajar-round-xlarge fajar-small">
            <div class="fajar-container fajar-center fajar-round-xlarge fajar-teal" style="width:60%">
              <div class="fajar-center fajar-text-white">60%</div>
            </div>
          </div>
          <p>Database MySQL</p>
          <div class="fajar-light-grey fajar-round-xlarge fajar-small">
            <div class="fajar-container fajar-center fajar-round-xlarge fajar-teal" style="width:80%">80%</div>
          </div>
          <p>Database PostgreSQL</p>
          <div class="fajar-light-grey fajar-round-xlarge fajar-small">
            <div class="fajar-container fajar-center fajar-round-xlarge fajar-teal" style="width:70%">70%</div>
          </div>
          <br>

          <p class="fajar-large fajar-text-theme"><b><i class="fa fa-globe fa-fw fajar-margin-right fajar-text-teal"></i>Languages</b></p>
          <p>English</p>
          <div class="fajar-light-grey fajar-round-xlarge">
            <div class="fajar-round-xlarge fajar-teal" style="height:24px;width:55%"></div>
          </div>
          <p>Indonesian</p>
          <div class="fajar-light-grey fajar-round-xlarge">
            <div class="fajar-round-xlarge fajar-teal" style="height:24px;width:100%"></div>
          </div>
          <p>Javaneese</p>
          <div class="fajar-light-grey fajar-round-xlarge">
            <div class="fajar-round-xlarge fajar-teal" style="height:24px;width:75%"></div>
          </div>
          <br>
        </div>
      </div><br>

    <!-- End Left Column -->
    </div>

    <!-- Right Column -->
    <div class="fajar-twothird">
    
      <div class="fajar-container fajar-card fajar-white fajar-margin-bottom">
        <h2 class="fajar-text-grey fajar-padding-16"><i class="fa fa-suitcase fa-fw fajar-margin-right fajar-xxlarge fajar-text-teal"></i>Work Experience</h2>
        <div class="fajar-container">
          <h5 class="fajar-opacity"><b>C# and PHP Programmer / Pura Smart Teknologi</b></h5>
          <h6 class="fajar-text-teal"><i class="fa fa-calendar fa-fw fajar-margin-right"></i>Maret 2019 - <span class="fajar-tag fajar-teal fajar-round">Current</span></h6>
          <p>Membuat program aplikasi sesuai request dari user yang fungsinya yaitu untuk mempercepat sebuah pekerjaan dari user tersebut.</p>
          <hr>
        </div>
        <div class="fajar-container">
          <h5 class="fajar-opacity"><b>IT Officer / PT. Kanaan Global Indonesia</b></h5>
          <h6 class="fajar-text-teal"><i class="fa fa-calendar fa-fw fajar-margin-right"></i>Maret 2018 - Februari 2019</h6>
          <p>Mengatasi permasalahan di perusahaan yang berhubungan dengan permasalahan jaringan komputer atau troubleshooting komputer yang ada di dalam perusahaan tersebut.</p>
          <hr>
        </div>
      </div>

      <div class="fajar-container fajar-card fajar-white">
        <h2 class="fajar-text-grey fajar-padding-16"><i class="fa fa-institution fa-fw fajar-margin-right fajar-xxlarge fajar-text-teal"></i>Education</h2>
        <div class="fajar-container">
          <h5 class="fajar-opacity"><b>SMK NU Ma'arif Kudus</b></h5>
          <h6 class="fajar-text-teal"><i class="fa fa-calendar fa-fw fajar-margin-right"></i>2008-2011</h6>
          <p>Machine Engineering</p>
          <hr>
        </div>
        <div class="fajar-container">
          <h5 class="fajar-opacity"><b>Universitas Muria Kudus</b></h5>
          <h6 class="fajar-text-teal"><i class="fa fa-calendar fa-fw fajar-margin-right"></i>2013 - 2018</h6>
          <p>Sstem Informasi</p>
          <hr>
        </div>
      </div><p>
      <div class="fajar-container fajar-card fajar-white">
        <h2 class="fajar-text-grey fajar-padding-16"><i class="fa fa-lightbulb-o fa-fw fajar-margin-right fajar-xxlarge fajar-text-teal"></i>Prestasi</h2>
        <div class="fajar-container">
          <h5 class="fajar-opacity"><b>Pekan Ilmiah Mahasiswa Nasional ke 29 Institut Pertanian Bogor</b></h5>
          <h6 class="fajar-text-teal"><i class="fa fa-calendar fa-fw fajar-margin-right"></i>2016</h6>
          <hr>
        </div>
      </div>

    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
  <!-- End Page Container -->
</div>

<footer class="fajar-container fajar-teal fajar-center fajar-margin-top">
  <p>&copy; Copyright by : Anis Fajar Prakoso </p>
</footer>

</body>
</html>
